package carro;
import java.util.Date;

public class Carro {
    public String color;
    public String marca;
    public String tipo;
    public int modelo;
    public String gasolina;
    public byte puertas;
    public Date anioCompra;

    public Carro(String color, String marca, String tipo, int modelo, String gasolina, byte puertas, Date anioCompra) {
        this.color = color;
        this.marca = marca;
        this.tipo = tipo;
        this.modelo = modelo;
        this.gasolina = gasolina;
        this.puertas = puertas;
        this.anioCompra = anioCompra;
    
    }

    public Carro() {
       
    }
     
    public String getColor() {
        return color;
    }

    public String getMarca() {
        return marca;
    }

    public String getTipo() {
        return tipo;
    }

    public int getModelo() {
        return modelo;
    }

    public String getGasolina() {
        return gasolina;
    }

    public byte getPuertas() {
        return puertas;
    }
    
    public Date getAnioCompra(){
        return anioCompra;
    }

}
    

    