/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package DAO;

/**
 *
 * @author David Noguera y Samuel Bolaños
 * Aqui se busca implementar un patron DAO con el fin de encapsular la logica de acceso de los datos
 * Se hace con para evitar un acceso directo a los datos por parte de amenzas. 
 */
public interface DAOLogin {
    
    public boolean VerificarUsuarios(String usuario, String contrasenia);
    public String getUsuario();
    public boolean CambiarContrasenia(String contrasenia);
    
}
