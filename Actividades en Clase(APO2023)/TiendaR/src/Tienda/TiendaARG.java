/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package Tienda;

import java.net.Socket;
import javax.swing.JOptionPane;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 * Librerias que se van a utilizar para el archivo binario
 */
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.ArrayList;


/**
 *
 * @author David Noguera
 */


public class TiendaARG extends javax.swing.JFrame implements Serializable {
    
    FondoPanel fondo = new FondoPanel();
    
   String productos [] = {"Mate","Alfajor","Choripan","Manaos Uva","Manaos Coca", "Autografo de Messi"};
   double precios []= {3,1,5,4,4,4000};
   double precio = 0;
   int cantidad = 0;
   DefaultTableModel modelo = new DefaultTableModel ();
   ArrayList<Venta> listaVentas = new ArrayList<Venta>();
   
    public TiendaARG() { 
        this.setContentPane(fondo);
        initComponents();
        this.setTitle("Tienda argentina PAPA");
        Image icono=Toolkit.getDefaultToolkit().getImage(getClass().getResource("/imagenes/messi.png"));
        this.setIconImage(icono);
        this.setLocationRelativeTo(null);
        this.setSize(443, 424);
        DefaultComboBoxModel comboModel = new DefaultComboBoxModel(productos);
        cboproducto.setModel (comboModel);
        calcularPrecio();
        modelo.addColumn("Descripcion");
        modelo.addColumn("Precio U");
        modelo.addColumn("Cantidad");
        modelo.addColumn("Importe");
        actualizarTabla();
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        cboproducto = new javax.swing.JComboBox<>();
        spncantidad = new javax.swing.JSpinner();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblProductos = new javax.swing.JTable();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        lblimporte = new javax.swing.JLabel();
        lbltotal = new javax.swing.JLabel();
        lbliva = new javax.swing.JLabel();
        lblsubtotal = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        lblprecio1 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(204, 255, 255));
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Impact", 0, 24)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("ARGENTIENDA");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 10, -1, -1));

        jLabel2.setText("CANTIDAD");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 80, -1, -1));

        jLabel3.setText("IVA");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 320, -1, -1));

        jLabel4.setText("PRODUCTO");
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 50, -1, -1));

        jButton1.setText("AGREGAR");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 60, -1, -1));

        cboproducto.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cboproducto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboproductoActionPerformed(evt);
            }
        });
        getContentPane().add(cboproducto, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 50, -1, -1));

        spncantidad.setModel(new javax.swing.SpinnerNumberModel(1, 1, 10, 1));
        spncantidad.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spncantidadStateChanged(evt);
            }
        });
        getContentPane().add(spncantidad, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 80, 80, -1));

        tblProductos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblProductos);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 120, 350, 120));

        jLabel6.setText("TOTAL");
        getContentPane().add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 340, -1, -1));

        jLabel7.setText("SUBTOTAL");
        getContentPane().add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 300, -1, -1));

        lblimporte.setText("$0.00");
        getContentPane().add(lblimporte, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 80, -1, -1));

        lbltotal.setText("$0.00");
        getContentPane().add(lbltotal, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 340, -1, -1));

        lbliva.setText("$0.00");
        getContentPane().add(lbliva, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 320, -1, -1));

        lblsubtotal.setText("$0.00");
        getContentPane().add(lblsubtotal, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 300, -1, -1));

        jLabel8.setText("PRECIO");
        getContentPane().add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 50, -1, -1));

        lblprecio1.setText("$0.00");
        getContentPane().add(lblprecio1, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 50, -1, -1));

        jLabel5.setText("IMPORTE");
        getContentPane().add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 80, -1, -1));

        jButton2.setText("CARGAR");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 330, -1, -1));

        jButton3.setText("GUARDAR");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton3, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 290, -1, -1));

        jMenuBar1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)));
        jMenuBar1.setForeground(new java.awt.Color(255, 255, 255));

        jMenu1.setText("File");
        jMenuBar1.add(jMenu1);

        jMenu2.setText("Edit");
        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        Venta venta = new Venta();
        venta.setId(cboproducto.getSelectedIndex());
        venta.setDescripcion(cboproducto.getSelectedItem().toString());
        venta.setPrecio(precio);
        venta.setCantidad(cantidad);
        venta.setImporte(precio*cantidad);
        if (!buscarVenta(venta)){listaVentas.add(venta);}
        actualizarTabla();
        borrar();
    }//GEN-LAST:event_jButton1ActionPerformed
    
    
    
    private void cboproductoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboproductoActionPerformed
        calcularPrecio();
    }//GEN-LAST:event_cboproductoActionPerformed

    private void spncantidadStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spncantidadStateChanged
        calcularPrecio();
    }//GEN-LAST:event_spncantidadStateChanged

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        guardarDatosEnArchivo();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        cargarDatosDesdeArchivo();
    }//GEN-LAST:event_jButton2ActionPerformed

    public void calcularPrecio(){
    
    precio = precios [cboproducto.getSelectedIndex()];
    cantidad = Integer.parseInt(spncantidad.getValue().toString());
    lblprecio1.setText(aMoneda(precio));
    lblimporte.setText(aMoneda(precio*cantidad));
    }
    
    public String aMoneda (double precio){
        return "$"+Math.round(precio*100.0)/100.0;
    }
    
    public boolean buscarVenta(Venta nueva){
     
        for (Venta v : listaVentas){
            if (v.getId()==nueva.getId()){
                int nuevaCantidad=v.getCantidad()+nueva.getCantidad();
                v.setCantidad(nuevaCantidad);
                v.setImporte(v.getPrecio()*nuevaCantidad);
                return true;
        }
            
        }return false;
    }
    
    public void borrar(){
    
        precio =0;
        cantidad =1;
        cboproducto.setSelectedIndex(0);
        spncantidad.setValue(1);
        calcularPrecio();

    }
    
    public void actualizarTabla(){
    
    while(modelo.getRowCount()>0){
        modelo.removeRow(0);   
    }
    double subtotal=0;
    for (Venta v : listaVentas){
        Object x []= new Object [4];
        x[0]=v.getDescripcion();
        x[1]=aMoneda(v.getPrecio());
        x[2]=v.getCantidad();
        x[3]=aMoneda(v.getImporte());
        subtotal+= v.getImporte();
        modelo.addRow(x);
    }
    
    double iva =subtotal*0.19;
    double total= subtotal+iva;
    lblsubtotal.setText(aMoneda(subtotal));
    lbliva.setText(aMoneda(iva));
    lbltotal.setText(aMoneda(total));
    tblProductos.setModel(modelo);
        
    } 
      
    
    /**
     * A continuacion se añaden los metodos que me permitiran guardar los datos en mi arhivo Binario datos.bin
     * 
     * guardarDatosEnArchivo se encanga de mandar la lista de articulos que se realizan en la ultima compra osea cuando presiono el boton "Guardar"
     * ultimaV significa ultima venta
     * 
     * cargarDatosDesdeArchivo se encarga de cargar la lista de articulos previamente guardados y este se activa al momento de darle al boton "Cargar"
     * registroU significa ultimo registro
     * 
     * Se emplean try n catch junto OOS ObjectOutputStream y FOS FileOutputStream
     */
    
    private void guardarDatosEnArchivo() {
        
            
            try {
                FileOutputStream archivo = new FileOutputStream("datos.bin");
                ObjectOutputStream ultimaV = new ObjectOutputStream(archivo);
                ultimaV.writeObject(listaVentas);
                ultimaV.close();
                JOptionPane.showMessageDialog(null, "Los datos se guardaron en el archivo 'datos.bin'.");
                } 
                catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Error al guardar los datos en el archivo.");
             }
        }       
        
        private void cargarDatosDesdeArchivo() {
            try {
                FileInputStream registro = new FileInputStream("datos.bin");
                ObjectInputStream registroU = new ObjectInputStream(registro);
                listaVentas = (ArrayList<Venta>) registroU.readObject();
                registroU.close();
                actualizarTabla();
                 JOptionPane.showMessageDialog(null, "Los datos se cargaron desde el archivo 'datos.bin'.");
                 }
                catch (Exception e) {
                 JOptionPane.showMessageDialog(null, "Error al cargar los datos desde el archivo.");
             }   
        }
    
    /**
     * Este metodo me permite hacer que mi tienda solo funcione cuando activo mi servidor en este caso serverRMI se emplea sockets y se checkea el estado
     * de mi servidor
     */
    
    
    public static void main(String args[]) {
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            
            public void run() {
                
                boolean serverIsActive = checkServerStatus("127.0.0.1", 7777);
        
             if (serverIsActive) {
            
             new TiendaARG().setVisible(true);
             } 
             else {
            
            JOptionPane.showMessageDialog(null, "El servidor no está activo");
            }
             }
    
            private static boolean checkServerStatus(String ipAddress, int port) {
            try {
             Socket socket = new Socket(ipAddress, port);
             socket.close();
             return true;
             } catch (Exception e) {
             return false;
         }
     }
    
            
        });
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> cboproducto;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblimporte;
    private javax.swing.JLabel lbliva;
    private javax.swing.JLabel lblprecio1;
    private javax.swing.JLabel lblsubtotal;
    private javax.swing.JLabel lbltotal;
    private javax.swing.JSpinner spncantidad;
    private javax.swing.JTable tblProductos;
    // End of variables declaration//GEN-END:variables

    
    /**
     * Aqui me ayuda a que mi imagen de fondo se reescale con la interfas
     */
    
    class FondoPanel extends JPanel
    
    {
    
       private Image imagen;
        
        @Override
        public void paint(Graphics g)
        {
            imagen = new ImageIcon(getClass().getResource("/imagenes/argban.jpg")).getImage();
            
            g.drawImage(imagen,0, 0, getWidth(), getHeight(),this);
            
            setOpaque(false);
            
            super.paint(g);
        }
        
    
    }
    
    
    
    

}


