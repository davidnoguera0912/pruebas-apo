/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package clientermi;

/**
 *
 * @author sistemas
 */

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import Interface.RMIDAO;

public class ClienteRMI {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        
        
        
        try{
            Registry registro=LocateRegistry.getRegistry("127.0.0.1",7777);
            RMIDAO interfaz = (RMIDAO) registro.lookup("RemotoRMI");
            int suma;
            suma = interfaz.sumar(6, 14);
            System.out.println("suma: "+suma);
        }catch(Exception e){
            System.out.println(""+e);
            
        }
        
        try {
            Registry registro=LocateRegistry.getRegistry("127.0.0.1",7777);
            RMIDAO interfaz = (RMIDAO) registro.lookup("RemotoRMI");
            int resta;
            resta = interfaz.restar(8, 2);
            System.out.println("resta: "+resta);
        
        } catch (Exception e){
            System.out.println(""+e);
        }
        
        try {
            Registry registro=LocateRegistry.getRegistry("127.0.0.1",7777);
            RMIDAO interfaz = (RMIDAO) registro.lookup("RemotoRMI");
            int multi;
            multi = interfaz.multiplicar(7, 5);
            System.out.println("multiplicacion: "+multi);
        
        } catch (Exception e){
            System.out.println(""+e);
        }
        
        try {
            Registry registro=LocateRegistry.getRegistry("127.0.0.1",7777);
            RMIDAO interfaz = (RMIDAO) registro.lookup("RemotoRMI");
            float div;
            div = interfaz.dividir(4,  2);
            System.out.println("division: "+div);
        
        } catch (Exception e){
            System.out.println(""+e);
        }
        
        try {
            Registry registro=LocateRegistry.getRegistry("127.0.0.1",7777);
            RMIDAO interfaz = (RMIDAO) registro.lookup("RemotoRMI");
            String men;
            men = interfaz.Mensaje("Mi prueba de servidor ");
            System.out.println(men);
        
        } catch (Exception e){
            System.out.println(""+e);
        }
        
        
        
    }
    
}
