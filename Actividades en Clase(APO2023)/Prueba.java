/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package prueba;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 *
 * @author Acer
 */
public class Prueba {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
    String nombreArchivo = "archivo.bin";

        // Escribir en el archivo binario
        try (DataOutputStream salida = new DataOutputStream(new FileOutputStream(nombreArchivo))) {
            int entero = 10;
            double decimal = 3.14;
            String texto = "Hola, mundo!";
            
            salida.writeInt(entero);
            salida.writeDouble(decimal);
            salida.writeUTF(texto);
            
            System.out.println("Datos escritos en el archivo binario.");
        } catch (IOException e) {
            System.out.println("Error al escribir en el archivo binario: " + e.getMessage());
        }

        // Leer desde el archivo binario
        try (DataInputStream entrada = new DataInputStream(new FileInputStream(nombreArchivo))) {
            int enteroLeido = entrada.readInt();
            double decimalLeido = entrada.readDouble();
            String textoLeido = entrada.readUTF();
            
            System.out.println("Entero leído: " + enteroLeido);
            System.out.println("Decimal leído: " + decimalLeido);
            System.out.println("Texto leído: " + textoLeido);
        } catch (IOException e) {
            System.out.println("Error al leer el archivo binario: " + e.getMessage());
        }
    }
}
